package kz.astana.notificationapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

public class MainActivity extends AppCompatActivity {

    private final String CHANNEL_ID = "CHANNEL_ID";
    private final int REQUEST_CODE = 100;
    private final String GROUP_KEY = "GROUP_KEY";
    private int NOTIFY_ID = 101;
    private int NOTIFY_ID_CUSTOM = 201;
    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        createChannel();

        Button createNotificationButton = findViewById(R.id.createNotificationButton);
        createNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNotification();
            }
        });

        Button createCustomNotificationButton = findViewById(R.id.createCustomNotificationButton);
        createCustomNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCustomNotification();
            }
        });

        Button clearNotificationButton = findViewById(R.id.clearNotificationButton);
        clearNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationManager.cancel(NOTIFY_ID);
            }
        });

        Button clearAllNotificationButton = findViewById(R.id.clearAllNotificationButton);
        clearAllNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationManager.cancelAll();
            }
        });
    }

    private void createChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    CHANNEL_ID, "First channel", NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.setDescription("First channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void createNotification() {
        Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                MainActivity.this,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        String bigText = "dsalkfaksjglajsgkhsakgljhaskfhgkjafghasdkghkdsahgkghsdakghaskdjsdhgkalshgksagkhdsakgjhsakgh" +
                "lkdsjfkasjdlagslkjsaglkjsalkghlkasdhgklhsagkhsadkghaskdghksalkcsamlksajkjaesfmn,mandflasdfkajdslfjas" +
                "kjdsahfkjsahdkfhsadkhfksahdfkhsadkfhaskldhfkashflkashdflkashflksahfkashflsahdlfhasdhfkashflkashflksaj";
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_heart, options);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        for (int i = 0; i < 10; i++) {
            inboxStyle.addLine("Line " + i);
        }

        NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle("You");
        messagingStyle.setConversationTitle("Chat");
        messagingStyle.addMessage("Hello", System.currentTimeMillis(), "He");
        messagingStyle.addMessage("How are you?", System.currentTimeMillis(), "She");
        messagingStyle.addMessage("Fine", System.currentTimeMillis(), "You");
        messagingStyle.addMessage("Well", System.currentTimeMillis(), "He");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this, CHANNEL_ID);
        builder
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_heart))
                .setAutoCancel(true)
                .setContentTitle("Title")
                .setContentText("Notification")
                .setContentInfo("Text")
                .setColor(Color.BLACK)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(bigText))
//                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
//                .setStyle(inboxStyle)
//                .setStyle(messagingStyle)
                .setContentIntent(pendingIntent)
                .setGroup(GROUP_KEY)
                .setGroupSummary(true);

        Notification notification = builder.build();
        notificationManager.notify(NOTIFY_ID, notification);
        NOTIFY_ID++;
    }

    private void createCustomNotification() {
        Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                MainActivity.this,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.layout_notification);
        remoteViews.setTextViewText(R.id.notificationTextView, "Custom text");
        remoteViews.setOnClickPendingIntent(R.id.root, pendingIntent);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setAutoCancel(true);
        builder.setContent(remoteViews);

        Notification notification = builder.build();
        notificationManager.notify(NOTIFY_ID_CUSTOM, notification);
    }
}